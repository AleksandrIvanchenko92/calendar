import moment from 'moment';
import { useEffect, useState } from 'react';
import CalendarGrid from '../CalendarGrid/CalendarGrid';
import { ButtonsWrapper, ButtonWrapper, EventBody, EventText, EventTitle, FormWrapper, FromPositionWrapper, ShadowWrapper, StyledTimePicker } from '../containers/StyledComponents';
import DayShowComponent from '../DayShowComponent/DayShowComponent';
import { DISPLAY_DAY, DISPLAY_MONTH } from '../helpers/constants';
import Monitor from '../Monitor/Monitor';
import YearSelectComponent from '../YearSelectComponent/YearSelectComponent';

const url = process.env.API_URL ?  process.env.API_URL : 'http://localhost:5000/';
const totalDays = 42;

const defaultEvent = {
  title: '',
  description: '',
  date: moment().format('X'),
  time: '00: 00',
}

function App() {
  const [displayMode, setDisplayMode] = useState(localStorage.getItem('displayMode') || DISPLAY_MONTH);
  moment.updateLocale('en', {week: {dow: 1}});
  const [today, setToday] = useState(moment(localStorage.getItem('currentDate') || undefined)); 
  const startDay = today.clone().startOf(DISPLAY_MONTH).startOf('week');
  const [events, setEvents] = useState([]);
  const [event, setEvent] = useState(null);
  const [isShowForm, setIsShowForm] = useState(false);
  const [method, setMethod] = useState(null);
  const [isShowYearSelect, setIsShowYearSelect] = useState(false);
  const startDayQuery = startDay.clone().format('X');
  const endDayQuery = startDay.clone().add(totalDays, 'days').format('X');

  const prevHandler = () => {
    const newDate = today.clone().subtract(1, displayMode);
    localStorage.setItem('currentDate', newDate);
    setToday(newDate);
  };

  const todayHandler = () => {
    const newDate = moment();
    moment.updateLocale('en', {week: {dow: 1}});
    localStorage.setItem('currentDate', newDate); 
    setToday(newDate);
  };

  const nextHandler = () => {
    const newDate = today.clone().add(1, displayMode);
    localStorage.setItem('currentDate', newDate);  
    setToday(newDate);
  };

  
  const changeDispayMode = (displayMode) => {
    localStorage.setItem('displayMode', displayMode);
    setDisplayMode(displayMode);
  };

  const openFormHandler = (methodName, eventForUpdate, dayItem) => {
    setEvent(eventForUpdate || {...defaultEvent, date: dayItem.format('X')});
    setMethod(methodName);
  };

  const openModalFormHandler = (methodName, eventForUpdate, dayItem) => {
    setIsShowForm(true);
    openFormHandler(methodName, eventForUpdate, dayItem);
  };

  const cancelButtonHandler = () => {
    setIsShowForm(false)
    setEvent(null)
  };

  useEffect(() => {
    fetch(`${url}events?date_gte=${startDayQuery}&date_lte=${endDayQuery}`)
      .then(res => res.json())
      .then(res => {
        setEvents(res);
      })
  }, [endDayQuery, startDayQuery, today])

  const chengeEventHandler = (text, field) => {
    setEvent(prevState => ({
      ...prevState,
      [field]: text
    }))
  }

  const eventFetchHandler = () => {
    if (!event.title.trim() || !event.description.trim()) {
      return;
    }
    
    const fetchUrl = method === 'Update' ? `${url}events/${event.id}` : `${url}events`;
    const httpMethod = method === 'Update' ? 'PATCH' : 'POST';

    fetch(fetchUrl, {
      method: httpMethod,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(event)
    })
    .then(res => res.json())
    .then(res => {
      if(method === 'Update') {
        setEvents(prevState => prevState.map(eventEl => eventEl.id === res.id ? res: eventEl ))
      } else {
        setEvents(prevState => [...prevState, res]);
      }
      cancelButtonHandler()
    })
  }

  const removeEventHandler = () => {
    const fetchUrl = `${url}events/${event.id}`;
    const httpMethod = 'DELETE';

    fetch(fetchUrl, {
      method: httpMethod,
      headers: {
        'Content-Type': 'application/json'
      },
    })
    .then(res => res.json())
    .then(res => {
      setEvents(prevState => prevState.filter(eventEl => eventEl.id !== event.id ))
      cancelButtonHandler()
    })
  }

  return (
    <>
      {
        isShowForm ? (
          <FromPositionWrapper onClick={cancelButtonHandler}>
            <FormWrapper onClick={e => e.stopPropagation()}>
              <EventText>Title</EventText>
              <EventTitle 
                value={event.title}
                onChange={e => chengeEventHandler(e.target.value, 'title')}
              />
              <EventText>Description</EventText>
              <EventBody 
                value={event.description}
                onChange={e => chengeEventHandler(e.target.value, 'description')}
              />
              <EventText>Time</EventText>
              <StyledTimePicker
                value={event.time}
                onChange={time => chengeEventHandler(time, 'time')}
                disableClock={true}
              />
              <ButtonsWrapper>
                <ButtonWrapper
                  onClick={cancelButtonHandler}
                >
                  Cancel
                </ButtonWrapper>
                <ButtonWrapper
                  onClick={eventFetchHandler}
                  title={'SAVE'}
                >
                  {method}
                </ButtonWrapper>
                {method === 'Update' ? (
                  <ButtonWrapper
                    danger
                    onClick={removeEventHandler}
                  >
                    Remove
                  </ButtonWrapper>
                  ) : null
                }
                
              </ButtonsWrapper>
            </FormWrapper>
          </FromPositionWrapper>
        ): null
      }
      {
        isShowYearSelect ? (
          <YearSelectComponent
            setIsShowYearSelect={setIsShowYearSelect}
            setToday={setToday}
          />
        ): null
      }
      <ShadowWrapper>
        <Monitor 
          today={today}
          prevHandler={prevHandler}
          todayHandler={todayHandler}
          nextHandler={nextHandler}
          setDisplayMode={changeDispayMode}
          displayMode={displayMode}
          setIsShowYearSelect={setIsShowYearSelect}
        />
       
        {
          displayMode === DISPLAY_MONTH ? (
            <CalendarGrid 
              startDay={startDay} 
              today={today} 
              totalDays={totalDays}
              events={events}
              openFormHandler={openModalFormHandler}
              setDisplayMode={changeDispayMode}
            />
          ) : null
        }
        {
          displayMode === DISPLAY_DAY ? (
           <DayShowComponent 
            events={events}
            today={today}
            selectedEvent={event}
            chengeEventHandler={chengeEventHandler}
            cancelButtonHandler={cancelButtonHandler}
            eventFetchHandler={eventFetchHandler}
            removeEventHandler={removeEventHandler}
            method={method}
            openFormHandler={openFormHandler}
          />
          ) : null
        }
      </ShadowWrapper>
    </>
  );
}

export default App;
