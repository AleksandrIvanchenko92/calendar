import React from 'react'
import CalendarGridHeader from '../CalendarGridHeader/CalendarGridHeader';
import { GridWrapper } from '../containers/StyledComponents';
import { MonthDaysList } from '../MonthDaysList/MonthDayList';

export default function CalendarGrid({ 
  startDay,
  today,
  totalDays,
  events, 
  openFormHandler,
  setDisplayMode 
}) {
  return (
    <>
    <GridWrapper isHeader isSelectedMonth>
      <CalendarGridHeader />
    </GridWrapper>
    <GridWrapper>
      <MonthDaysList
        totalDays={totalDays}
        openFormHandler={openFormHandler}
        events={events}
        startDay={startDay}
        today={today}
        setDisplayMode={setDisplayMode}
      />
    </GridWrapper>
    </>
   
  )
}