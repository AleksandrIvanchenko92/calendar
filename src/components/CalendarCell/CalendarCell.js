import React from "react";
import { CellWrapper, CurrentDay, DateWrapper, DayWrapper, EventItemWrapper, EventListItemWrapper, EventListWrapper, RowInCell, ShowDateWrapper } from "../containers/StyledComponents";
import { isCurrentDay, isSelectedMonth } from "../helpers";
import { DISPLAY_DAY } from '../helpers/constants';

export const CalendarCell = ({
  dayItem,
  today,
  openFormHandler,
  events,
  setDisplayMode
}) => {
  
  return (
    <CellWrapper
    key={dayItem.unix()}
    isWeekend={dayItem.format('ddd') === 'Sat' || dayItem.format('ddd') === 'Sun'}
    isSelectedMonth={isSelectedMonth(dayItem, today)}
  >
    <RowInCell justifyContent={'flex-end'} >
      <ShowDateWrapper>
        <DayWrapper>
          {dayItem.format('ddd')} 
        </DayWrapper>
      
        <DateWrapper onDoubleClick={() => openFormHandler('Create', null, dayItem)}>
          {
            isCurrentDay(dayItem) ? (
              <CurrentDay>{dayItem.format('D')}</CurrentDay>
            ) : (
              dayItem.format('D')
            )
          }
        </DateWrapper>
      </ShowDateWrapper>
        <EventListWrapper>
            {
              events
              .slice(0, 2)
              .map(event => (
                <EventListItemWrapper key={event.id}>
                  <EventItemWrapper onDoubleClick={() => openFormHandler('Update', event)}>
                    {event.title}
                  </EventItemWrapper>
                </EventListItemWrapper>
              ))
            }
            {
              events.length > 2 ? (
                <EventListItemWrapper key='show more'>
                  <EventItemWrapper onClick={() => setDisplayMode(DISPLAY_DAY)}>
                    show more...
                  </EventItemWrapper>
              </EventListItemWrapper>
              ) : null
            }
        </EventListWrapper>
    </RowInCell>
  </CellWrapper>
  )
}