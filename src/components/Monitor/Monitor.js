import React from 'react'
import { ButtonsWrapper, DivWrapper, TextWrapper, TitleWrapper, TodayButton, TodayButtonWrapper } from '../containers/StyledComponents'
import { DISPLAY_DAY, DISPLAY_MONTH } from '../helpers/constants'

export default function Monitor({
  today, 
  prevHandler, 
  todayHandler, 
  nextHandler,
  setDisplayMode,
  displayMode,
  setIsShowYearSelect
}) {

  return (
    <DivWrapper>
      <div>
        {
          displayMode === DISPLAY_DAY ? (
            <TextWrapper>{today.format('DD')}</TextWrapper>
          ) : null
        }
        <TitleWrapper>{today.format('MMMM')}</TitleWrapper>
        <TextWrapper>{today.format('YYYY')}</TextWrapper>
      </div>
    
      <ButtonsWrapper>
        <TodayButton onClick={() => setDisplayMode(DISPLAY_MONTH)}>Month</TodayButton>
        <TodayButton onClick={() => setDisplayMode(DISPLAY_DAY)}>Day</TodayButton>
      </ButtonsWrapper>
    
      <ButtonsWrapper>
        <span class="material-icons-outlined" onClick={() => setIsShowYearSelect(true)}>
          calendar_month
        </span>
        <TodayButtonWrapper onClick={prevHandler}> &lt; </TodayButtonWrapper>
        <TodayButton onClick={todayHandler}>Today</TodayButton>
        <TodayButtonWrapper onClick={nextHandler}> &gt; </TodayButtonWrapper>
      </ButtonsWrapper>
    </DivWrapper>
  )
}
