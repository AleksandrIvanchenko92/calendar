import styled from "styled-components";
import TimePicker from 'react-time-picker';

export const CellWrapper = styled.div`
  min-width: 140px;
  min-height: ${props => props.isHeader ? 24 : 94}px;
  background-color: ${props => props.isWeekend ? '#66A5AD' : '#C4DFE6'}; 
  color: ${props => props.isSelectedMonth ? '#003B46' : '#1995AD'};
  font-weight: bold;
`;

export const RowInCell = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: ${props => props.justifyContent ? props.justifyContent : 'flex-start'};
  ${props => props.pr && `padding-right: ${props.pr * 8}px`}
`;

export const EventListWrapper = styled('ul')`
  margin: 0;
  padding: 0;
  list-style: none;
`;

export const EventListItemWrapper = styled('li')`
	padding-left: 2px;
	padding-right: 2px;
	margin-bottom: 2px;
	display: flex;
`;

export const EventItemWrapper = styled('button')`
	position: relative;
	flex-grow: 1;
	text-overflow: ellipsis;
	overflow: hidden;
	white-space: nowrap;
	width: 114px;
	border: unset;
	color: #003B46;
	cursor: pointer;
	margin: 0px;
	padding: 0px;
	text-align: left;
	background-color: #98cfdd;
	border: 1px solid #003B46;
	border-radius: 3px;
`;

export const EventTitle = styled('input')`
  padding: 4px 14px;
  font-size: .85rem;
  width: 100%;
  border: unset;
  background-color: #98cfdd;
  color: #003B46;
  outline: unset;
  border: 0.5px solid #003B46;
  border-radius: 5px;

`;

export const EventBody = styled('textarea')`
  padding: 4px 14px;
  font-size: .85rem;
  width: 100%;
  border: unset;
  background-color: #98cfdd;
  color: #003B46;
  outline: unset;
  border: 0.5px solid #003B46;
  resize: none;
  height: 60px;
  border-radius: 5px;
`;

export const ButtonWrapper = styled('button')`
  color: ${props => props.danger ? '#f00' : '#003B46'};
  border: 1px solid ${props => props.danger ? '#f00' : '#003B46'};
  border-radius: 5px;
  cursor: pointer;
  &:not(:last-child){
    margin-right: 2px;
  }
  &:hover { 
    background-color: #66A5AD;
    transition: all 0.5s ease;
  }
`;

export const TodayButtonWrapper = styled('button')`
  border: unset;
  background-color: #66A5AD;
  height: 20px;
  margin-right: 2px;
  border-radius: 4px;
  color: #C4DFE6;
  cursor: pointer;
`;

export const TodayButton = styled(TodayButtonWrapper)`
  padding-right: 16px;
  padding-left: 16px;
  font-weight: bold;
`;

export const CalendarButton = styled(TodayButtonWrapper)`
  background-color: #C4DFE6;
  color: #66A5AD;
  padding-right: 16px;
  padding-left: 16px;
  font-weight: bold;
`;

export const YearWrapper = styled('div')`
  display: grid;
  flex-wrap: wrap;
  gap: 10px;
  grid-template-columns: repeat(4, 1fr);
  margin: 0 auto;
`;

export const YearTitle = styled('div')`
  text-align: center;
  margin-bottom: 10px;
`;

export const MonthTitle = styled(YearTitle)`
  margin-bottom: 90px;
`;

export const BackArrow = styled('div')`
  position: absolute;
`;

export const DateWrapper = styled('div')`
  height: 33px;
  width: 33px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 2px;
  cursor: pointer;
`;

export const CurrentDay = styled('div')`
  height: 100%;
  width: 100%;
  background: #fff;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ShowDateWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;


export const DayWrapper = styled('div')`
  font-size: 7px;
  padding-left: 5px;
`;

export const GridWrapper = styled('div')`
  display: grid;
  grid-template-columns: repeat(7, 1fr);
  grid-gap: 1px;
  background-color:  ${props => props.isHeader ? '#C4DFE6' : '#07575B'};
  ${props => props.isHeader && 'border-bottom: 1px solid #07575B'}
`;

export const DayShowWrapper = styled('div')`
  display: flex;
  flex-grow: 1;
  border-top: 1px solid #003B46;
`;

export const EventsListWrapper = styled('div')`
  background-color: #66A5AD;
  color: #C4DFE6;
  flex-grow: 1;
  padding-top: 2px;
`;

export const EventFormWrapper = styled('div')`
  background-color: #66A5AD;
  color: #C4DFE6;
  width: 300px;
  position: relative;
  border-left: 2px solid #003B46;
`;

export const NoEventMsg = styled('div')`
  color: #C4DFE6;
  position: absolute;
  top: 50%;
  right: 50%;
  transform: translate(50%,-50%);
`;

export const DivWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  background-color: #C4DFE6;
  color: #66A5AD;
  padding: 6px;
`;

export const TextWrapper = styled('span')`
  font-size: 32px;
  color: #66A5AD;
`;

export const TitleWrapper = styled(TextWrapper)`
  font-weight: bold;
  margin-right: 10px;
`;

export const ButtonsWrapper = styled('div')`
  display: flex;
  align-items: center;
`;

export const ButtonEvent = styled('button')`
  font-size: 20px;
  color: #003B46;
  border-radius: 5px;
  background-color: #98cfdd;
  cursor: pointer;
  transition-duration: 0.8s;
  :hover {
    font-size: 100%;
    transform: scale(1.5)
  }
`;

export const ButtonEvContainer = styled('div')`
  padding-top: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const StyledTimePicker = styled(TimePicker)`
  margin-bottom: 15px;
`;

export const ShadowWrapper = styled('div')`
  min-width: 850px;
  height: 650px;
  display: flex;
  flex-direction: column;

  border-top: 1px solid #07575B;
  border-left: 1px solid #07575B;
  border-right: 1px solid #07575B;
  border-bottom: 1px solid #07575B;
  border-radius: 8px;
  overflow: hidden;
  box-shadow: 0px 0px 18px 8px #B9C4C9;
`;

export const FormWrapper = styled(ShadowWrapper)`
  width: 320px;
  min-width: 320px;
  height: 320px;
  background-color: #66A5AD;
  color: #003B46;
  box-shadow: unset;
  padding: 15px;
`;

export const FromPositionWrapper = styled('div')`
  position: absolute;
  z-index: 100;
  background-color: rgba(0, 0, 0, 0.35);
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  align-items: center;
  cursor: pointer;
  justify-content: center;
`;

export const EventText = styled('p')`
  display: flex;
  justify-content: center;
  color: #003B46;
  font-weight: bold;
`