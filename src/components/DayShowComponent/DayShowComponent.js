import React from 'react'
import { 
  ButtonEvContainer,
  ButtonEvent,
  ButtonsWrapper,
  ButtonWrapper,
  DayShowWrapper,
  EventBody,
  EventFormWrapper,
  EventItemWrapper,
  EventListItemWrapper,
  EventsListWrapper,
  EventTitle,
  NoEventMsg
} from '../containers/StyledComponents';
import { isDayContainCurrentEvent } from '../helpers'

export default function DayShowComponent({ 
  events,
  today,
  selectedEvent,
  chengeEventHandler,
  cancelButtonHandler,
  eventFetchHandler,
  removeEventHandler,
  method,
  openFormHandler
}) {
  const eventList = events.filter(event => isDayContainCurrentEvent(event, today,))
  return (
    <DayShowWrapper>
      <EventsListWrapper>
        <EventsListWrapper>
          {
            eventList.map(event => (
              <EventListItemWrapper key={event.id}>
                <EventItemWrapper onClick={() => openFormHandler('Update', event)}>
                  {event.title}
                </EventItemWrapper>
              </EventListItemWrapper>
            ))
          }
        </EventsListWrapper>
      </EventsListWrapper>
      <EventFormWrapper>
          {
            selectedEvent ? (
              <>
                <EventTitle 
                  placeholder={'Title'} 
                  value={selectedEvent.title}
                  onChange={e => chengeEventHandler(e.target.value, 'title')}
                />
                <EventBody
                  placeholder={'Description'}
                  value={selectedEvent.description}
                  onChange={e => chengeEventHandler(e.target.value, 'description')}
                />
                <ButtonsWrapper>
                  <ButtonWrapper
                    onClick={cancelButtonHandler}
                  >
                    Cancel
                  </ButtonWrapper>
                  <ButtonWrapper
                    onClick={eventFetchHandler}
                    title={'SAVE'}
                  >
                    {method}
                  </ButtonWrapper>
                  {method === 'Update' ? (
                    <ButtonWrapper
                      danger
                      onClick={removeEventHandler}
                    >
                      Remove
                    </ButtonWrapper>
                    ) : null
                  }
                  
                </ButtonsWrapper>
              </>
            ) : (
              <>
                <ButtonEvContainer>
                  <ButtonEvent  onClick={() => openFormHandler('Create', null, today)} >Create new event</ButtonEvent>
                </ButtonEvContainer>
                <NoEventMsg>No event selected</NoEventMsg>
              </>
            )
          }
      </EventFormWrapper>
    </DayShowWrapper>
  )
}
