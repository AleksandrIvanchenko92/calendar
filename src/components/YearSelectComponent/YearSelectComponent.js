import React, { useState } from 'react';
import moment from 'moment';
import { BackArrow, CalendarButton, FormWrapper, FromPositionWrapper, MonthTitle, YearTitle, YearWrapper } from '../containers/StyledComponents';

export default function YearSelectComponent({
  setIsShowYearSelect,
  setToday,
}){
  const [isShowMonthSelect, setIsShowMonthSelect] = useState(false);
  const [year, setYear] = useState('');

  const setMonth = (e) => {
    setIsShowYearSelect(false);
    moment.updateLocale('en', {week: {dow: 1}});

    setToday(prev => {
      const newDate = prev.clone().set('year', year).set('month', e.target.innerHTML);
      localStorage.setItem('currentDate', newDate);

      return newDate;
    });
  } 

  return (
    <FromPositionWrapper onClick={() => setIsShowYearSelect(false)}>
      <FormWrapper onClick={e => e.stopPropagation()}>
        {isShowMonthSelect 
          ? <MonthTitle> 
            Select {`month of ${year} year`} 
          </MonthTitle>
          : <YearTitle>
            Select year
          </YearTitle>
        }
        
        {!isShowMonthSelect 
          ? <YearWrapper>
            {[...Array(36)].map((_, i) => (
              <CalendarButton
                onClick={(e) => {
                  setYear(e.target.innerHTML);
                  setIsShowMonthSelect(true);
                }}
              >
                {moment().add(-18 + i, 'y').format('YYYY')}
              </CalendarButton>
            ))}
          </YearWrapper>
          : <>
            <BackArrow>
              <CalendarButton onClick={() => setIsShowMonthSelect(false)}> &lt; </CalendarButton>
            </BackArrow >
            <YearWrapper>
              {[...Array(12)].map((_, i) => (
                <CalendarButton onClick={setMonth}>
                  {moment().month(i + 1).format('MMM')}
                </CalendarButton>
              ))}
            </YearWrapper> 
          </>
        }
      </FormWrapper>
    </FromPositionWrapper>
  )
}
