# calendar

- [ ] [DEMO LINK](https://calendar-git-main-aleksandrod92-gmailcom.vercel.app/)

- [ ] [FRONTEND REPO](https://gitlab.com/AleksandrIvanchenko92/calendar-server)

- [ ] [SERVER REPO](https://gitlab.com/AleksandrIvanchenko92/calendar-server)

To create an event - double click on the date


In the project directory, you can run:

npm start

Runs the app in the development mode.

Dependencies:
Node v - 14.18.2
NVM v - 1.1.10

Technology: React, Heroku,  Json-server, Moment, styled-components